package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "tbl_colors")
public class Color {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String color;
	
	@ManyToOne
	@JoinColumn(name = "laptop_id")
	private Laptop laptop;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Laptop getLaptop() {
		return laptop;
	}

	public void setLaptop(Laptop laptop) {
		this.laptop = laptop;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Color() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Color(Long id, String color, Laptop laptop) {
		super();
		this.id = id;
		this.color = color;
		this.laptop = laptop;
	}

	@Override
	public String toString() {
		return "Color [id=" + id + ", color=" + color + ", laptop=" + laptop + "]";
	}

	
}
