package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.LaptopRequest;
import com.example.demo.dto.LaptopResponse;
import com.example.demo.entity.Brand;

import com.example.demo.entity.Laptop;
import com.example.demo.repo.BrandRepository;
import com.example.demo.repo.LaptopRepository;

@RestController
public class LaptopController {
	
	@Autowired
	private LaptopRepository lRepo;
	
	@Autowired
	private BrandRepository bRepo;
	
	@GetMapping("/laptops")
	public ResponseEntity<List<LaptopResponse>> getData () {
		List<Laptop> list = lRepo.findAll();
		List<LaptopResponse> responseList = new ArrayList<>();
		list.forEach(l -> {
			LaptopResponse response = new LaptopResponse();
			response.setId(l.getId());
			response.setLaptop(l.getName());
			response.setPrice(l.getPrice());
			response.setBrand(l.getBrand().getName());
			responseList.add(response);
		});
		return new ResponseEntity<>(responseList, HttpStatus.OK);
	}

	/*@PostMapping("/laptops/save")
	public ResponseEntity<Laptop> saveData (@RequestBody LaptopRequest req) {
		            
		Laptop laptop = new Laptop(req);
		laptop = lRepo.save(laptop);
	
			Brand b = new Brand();
			b.setName(laptop.getName());
			b.setId(laptop.getId());
			b.setLaptop(laptop);
			bRepo.save(b);
		
		return new ResponseEntity<>(laptop, HttpStatus.CREATED);
		
	}*/
}
