package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.BrandResponse;
import com.example.demo.entity.Brand;
import com.example.demo.repo.BrandRepository;

@RestController
public class BrandController {

	@Autowired
	private BrandRepository bRepo;
	
	@GetMapping("/brands")
	public List<BrandResponse> getAll () {
		List<Brand> list = bRepo.findAll();
		List<BrandResponse> responseList = new ArrayList<>();
		list.forEach(b -> {
			BrandResponse response = new BrandResponse();
			response.setId(b.getId());
			response.setBrand(b.getName());
			response.setLaptop(b.getLaptop().getName());
			response.setPrice(b.getLaptop().getPrice());
			responseList.add(response);
		});
		return responseList;
	}
}
